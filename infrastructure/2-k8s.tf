resource "yandex_iam_service_account" "netology-melentev-k8s-manager" {
  name        = "netology-melentev-k8s-manager"
}

resource "yandex_resourcemanager_folder_iam_member" "netology-melentev-editor" {
  # The service account is assigned the editor role.
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.netology-melentev-k8s-manager.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "netology-melentev-images-puller" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.netology-melentev-k8s-manager.id}"
}

resource "yandex_kubernetes_cluster" "netology-melentev-k8s-cluster" {
  network_id = yandex_vpc_network.netology-melentev-vpc.id
  name = "netology-melentev-k8s-cluster"

  master {
    version = "1.29"
    public_ip = true

    regional {
      region = "ru-central1"
      location {
        zone = yandex_vpc_subnet.netology-melentev-subnet-zone-a.zone
        subnet_id = yandex_vpc_subnet.netology-melentev-subnet-zone-a.id
      }
      location {
        zone = yandex_vpc_subnet.netology-melentev-subnet-zone-b.zone
        subnet_id = yandex_vpc_subnet.netology-melentev-subnet-zone-b.id
      }
      location {
        zone = yandex_vpc_subnet.netology-melentev-subnet-zone-d.zone
        subnet_id = yandex_vpc_subnet.netology-melentev-subnet-zone-d.id
      }
    }
  }

  service_account_id      = yandex_iam_service_account.netology-melentev-k8s-manager.id
  node_service_account_id = yandex_iam_service_account.netology-melentev-k8s-manager.id

  depends_on = [
    yandex_resourcemanager_folder_iam_member.netology-melentev-editor,
    yandex_resourcemanager_folder_iam_member.netology-melentev-images-puller
  ]
}

resource "yandex_kubernetes_node_group" "netology-melentev-node-group" {
  cluster_id = yandex_kubernetes_cluster.netology-melentev-k8s-cluster.id
  name = "netology-melentev-node-group"

  instance_template {
    platform_id = "standard-v1"

    resources {
      cores = 2
      memory = 2
    }

    boot_disk {
      size = 30
    }

    network_interface {
      subnet_ids = [
        yandex_vpc_subnet.netology-melentev-subnet-zone-a.id,
        yandex_vpc_subnet.netology-melentev-subnet-zone-b.id,
        yandex_vpc_subnet.netology-melentev-subnet-zone-d.id,
      ]
    }
  }

  allocation_policy {

    location {
      zone = yandex_vpc_subnet.netology-melentev-subnet-zone-a.zone
    }

    location {
      zone = yandex_vpc_subnet.netology-melentev-subnet-zone-b.zone
    }

    location {
      zone = yandex_vpc_subnet.netology-melentev-subnet-zone-d.zone
    }

  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }
}

