resource "yandex_vpc_network" "netology-melentev-vpc" {
  name = "netology-melentev-vpc"
}

resource "yandex_vpc_subnet" "netology-melentev-subnet-zone-a" {
  network_id     = yandex_vpc_network.netology-melentev-vpc.id
  name = "netology-melentev-subnet-zone-a"
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = "ru-central1-a"
}

resource "yandex_vpc_subnet" "netology-melentev-subnet-zone-b" {
  network_id     = yandex_vpc_network.netology-melentev-vpc.id
  name = "netology-melentev-subnet-zone-b"
  v4_cidr_blocks = ["192.168.11.0/24"]
  zone           = "ru-central1-b"
}

resource "yandex_vpc_subnet" "netology-melentev-subnet-zone-d" {
  network_id     = yandex_vpc_network.netology-melentev-vpc.id
  name = "netology-melentev-subnet-zone-d"
  v4_cidr_blocks = ["192.168.12.0/24"]
  zone           = "ru-central1-d"
}