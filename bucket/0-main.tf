terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "http" {}
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.default_zone
}

resource "yandex_storage_bucket" "netology-melentev-terraform-bucket" {
  bucket = "netology-melentev-terraform-bucket"
  access_key = var.access_key_bucket
  secret_key = var.secret_key_bucket
}